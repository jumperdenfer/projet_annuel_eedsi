<?php
return [
    'en' => [
        'display' => 'English',
        'flag-icon' => 'en'
    ],
    'fr' => [
        'display' => 'Français',
        'flag-icon' => 'fr'
    ],
];
<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ReferentielController;
use App\Http\Controllers\CategorieController;
use App\Http\Controllers\AuditController;
use App\Http\Controllers\LanguageController;
use App\Http\Controllers\QuestionController;
use App\Http\Controllers\ReponseController;
use App\Http\Controllers\RapportController;
use App\Http\Controllers\StatistiqueController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
require __DIR__.'/auth.php';

Route::get('/', function () {
    return view('welcome');
});

Route::get('/dashboard', function () {
    return view('dashboard');
})->middleware(['auth', 'verified'])->name('dashboard');


Route::prefix('users')->middleware(['auth'])->group(function () {

Route::get('lang/{lang}', [LanguageController::class, 'switchLang'])->name('lang.switch');

    //Get all referentiel for a specific user
    Route::get('/referentiels',[ReferentielController::class,'index'])->name('referentiels');
    Route::post('/referentiels/store',[ReferentielController::class,'store'])->name('referentiel.store');
    //Acces to edit a referentiel
    Route::get('/referentiels/{referentiel}',[ReferentielController::class,'show'])->name('referentiel.show');
    //Update the base information of a referentiel 
    Route::post('/referentiels/edit/{id}',[ReferentielController::class,'update'])->name("referentiel.edit");
    //Create a categorie for the referentiel 
    Route::post('/categories/store',[CategorieController::class,'store'])->name('categorie.store');
    Route::post('/categories/edit/{id}',[CategorieController::class,'update'])->name('categorie.edit');
    Route::post('/categories/delete/{id}',[CategorieController::class,'destroy'])->name('categorie.delete');
    //Route for CRUD question 
    Route::post('/question/store',[QuestionController::class,'store'])->name('question.store');
    Route::post('/question/edit/{id}',[QuestionController::class,'update'])->name('question.update');
    Route::post('/question/delete/{id}',[QuestionController::class,'destroy'])->name('question.delete');

    //Get all referentiel for a specific user
    Route::get('/audits',[AuditController::class,'index'])->name('audits');
    Route::post('/audits/store',[AuditController::class,'store'])->name('audit.store');
    //Acces to edit a referentiel
    Route::get('/audits/{audit}',[AuditController::class,'show'])->name('audit.show');
    Route::post('/audits/{audit}/commentaire',[AuditController::class,'commentaire'])->name('audit.commentaire');
    //Access to save or update a reponse
    Route::post('/reponses/store',[ReponseController::class,'store']);
    Route::get('/rapport/{audit}',[RapportController::class,'index'])->name('audit.rapport')->middleware('signed');

    //Access to statistique 
    Route::get('/statistiques',[StatistiqueController::class,'index'])->name('statistique.accueil');
    Route::get('/statistiques/{referentiel}',[StatistiqueController::class,'show'])->name('statistique.show');
});
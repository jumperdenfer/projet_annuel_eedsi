<?php

return [

    /*
    |--------------------------------------------------------------------------
    | French Language
    |--------------------------------------------------------------------------
    */

    // NAVIGATION
    'dashboard' => 'Dashboard',
    'users' => 'Users',
    'roles' => 'Roles',
    'permissions' => 'Permissions',
    'files.list' => 'Files list',

    //HOMEPAGE 

    'coverage.map' => 'Coverage Maps',
    'coverage.stats' => 'Coverage statistics',
    'coverage.maps.head' => 'Update coverage maps',
    'coverage.maps.message' => 'When integrating new coverage map, the site will be put in maintenance.',
    'coverage.stats.head' => 'Update coverage statistics',
    'coverage.stats.message' => 'When the new coverage statistics are integrated, the site will be put in maintenance.',

    // USER MENU
    'menu.profile' => 'Profile',
    'menu.logout' => 'Log out',

    // BUTTON
    'edit' => 'Edit',
    'delete' => 'Delete',
    'details' => 'Details',
    'send' => 'Send',
    'file.send' => 'Send',

    // USER PAGE
    'user.list' => 'Users list',
    'add.user' => 'Add user',
    'user.name' => 'Name',
    'user.email' => 'Email',
    'user.role' => 'Role',
    'user.password' => 'Password',
    'user.confirm.password' => 'Confirm password',

    //FILE LIST
    'file.list' => 'Files list',
    'file.name' => 'File name',
    'upload.by' => 'Upload by',
    'created_at' => 'Created at',
    'download' => 'Download'


];

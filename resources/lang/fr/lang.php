<?php

return [

    /*
    |--------------------------------------------------------------------------
    | French Language
    |--------------------------------------------------------------------------
    */

    // NAVIGATION

    'dashboard' => 'Tableau de bord',
    'gestion.ref' => 'Gestion des référentiels',
    'audits' => 'Audits',
    'stats' => 'Statistiques',
    'menu.logout' => 'Déconnexion',

    //DASHBOARD

    // REFERENTIEL

    'ref' => 'Referentiel :',
    'new.ref' => 'Nouveau Référentiel',
    'ref.libelle' => 'Libelle du référentiel :',
    'ref.description' => 'Description du référentiel :',
    'ref.add' => 'Ajouter nouveau référentiel',
    'ref.list' => 'Liste des référentiels :',
    'ref.created_at' => 'Créé le :',
    'ref.empty' => 'Aucun référentiel existant',
    'ref.edit' => 'Editer les informations du référentiel',
    'ref.add.category' => 'Ajouter une catégorie',
    'ref.lib.category' => 'Libelle de la catégorie :',
    'ref.save.category' => 'Enregistrer la catégorie',
    'ref.list.category' => 'Liste des catégorie :',
    'ref.lib.description' => 'Description de la catégorie :',
    'ref.category.coef' => 'Coefficient de la catégorie',
    'ref.category.empty' => 'Aucune catégorie',
    'category.libelle' => 'Nom de la catégorie :',
    'category.description' => 'Description la catégorie :',
    'add.question' => 'Ajouter une question',
    'edit.category' => 'Editer la catégorie',
    'libelle' => 'Libelle :',
    'description' => 'Description :',
    'coef' => 'Coefficient :',
    'save.modif' => 'Enregistrer les modification',
    'delete.category' => 'Supprimer la catégorie',
    'external.link' => 'Lien externe',
    'save.question' => 'Enregistrer la question',
    'question.empty' => 'Aucune question',
    'modif' => 'Modification',
    'modif.question' => 'Modifier la question',
    'delete.question' => 'Supprimer la question',

    // AUDITS

    'audit' => 'Audit :',
    'audit.gestion' => 'Gestion des audits',
    'audit.add' => 'Création d\un audit',
    'audit.email' => 'Email',
    'audit.date' => 'Date',
    'audit.list' => 'Liste des audits',
    'audit.add.ref' => 'Ajouter un nouvel audit',
    'audit.description' => 'Description',
    'audit.external.link' => 'Lien externe',
    'audit.note' => 'Note',
    'audit.commentaire' => 'Commentaire :',
    'audit.send' => 'Envoyer',
    'audit.send.audit' => 'Envoyer l\'audit',

    // ERRORS

    'errors.oups' => 'Oups!',
    'errors.message' => 'Quelque chose a mal tourné, veuillez vérifier les erreurs ci-dessous.',

];

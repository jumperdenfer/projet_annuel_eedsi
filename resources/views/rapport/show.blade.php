<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{__('Rapport')}} {{ $auditFull->referentiel->libelle }} - {{ $auditFull->predicted_at->format('d/m/Y') }}
        </h2>
        <div class="all-reset mt-5">
            {!! $auditFull->referentiel->description !!}
        </div>
    </x-slot>
    <div class="py-12 lg:max-w-screen-lg md:max-w-screen-md sm:max-w-screen-sm 2xl:max-w-screen-2xl m-auto">
        <div x-data="{categorie_show : 'bilan'}">
            <div class="space-x-4">
                {{-- Button rapport bilan --}}
                <div class="inline-block">
                    <button @click="categorie_show = 'bilan'" class="rounded bg-blue-700 text-white border border-black p-2 hover:bg-white hover:text-blue-700">
                        {{ __('Bilan')}}
                    </button>
                </div>
                {{-- Button Categorie --}}
                @foreach ($auditFull->referentiel->categories as $categorie) 
                    <div class="inline-block">
                        <button @click="categorie_show = {{$categorie->id}}"
                            class="rounded bg-blue-700 text-white border border-black p-2 hover:bg-white hover:text-blue-700" >
                            {{$categorie->libelle}}
                        </button>
                    </div>
                @endforeach
            </div>
            <hr class="my-3">
            <x-rapport.bilan class="bg-white p-5" x-show="categorie_show  == 'bilan'" :audit="$auditFull"></x-rapport.bilan>
            {{-- Block des catégories --}}
            @forelse ($auditFull->referentiel->categories as $categorie)
                <x-rapport.categorie class="bg-white p-5" x-show="categorie_show  == {{$categorie->id}}" :categorie="$categorie"></x-referentiels.rapport>
            @empty
                {{ __('Aucune catégorie')}}
            @endforelse
        </div>
    </div>

</x-app-layout>

<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('lang.gestion.ref') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8 bg-white shadow-lg rounded-lg py-4">
            {{-- Rendre cette partie en " caché "  --}}
            <div class="">
                @if (count($errors) > 0)
                <div class="bg-red-100 border-t-4 border-red-500 rounded-md text-red-900 px-4 py-3 shadow-md mb-4" role="alert">
                    <div class="flex">
                        <div class="py-1"><svg class="fill-current h-6 w-6 text-red-500 mr-4" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"><path d="M2.93 17.07A10 10 0 1 1 17.07 2.93 10 10 0 0 1 2.93 17.07zm12.73-1.41A8 8 0 1 0 4.34 4.34a8 8 0 0 0 11.32 11.32zM9 11V9h2v6H9v-4zm0-6h2v2H9V5z"/></svg></div>
                        <div>
                        <p class="font-bold">{{ __('lang.errors.oups') }}</p>
                        <p class="text-sm">{{ __('lang.errors.message') }}</p>
                        </div>
                    </div>
                </div>
                @endif
                <h2 class="text-2xl font-bold mb-2">{{ __('lang.new.ref') }}</h2>
                <form action="{{route('referentiel.store')}}" method="post">
                    @csrf
                    <div class="py-4">
                        <label class="block mb-1" for="libelle">{{ __('lang.ref.libelle') }}</label>
                        <input id="libelle" class="shadow-sm block sm:text-sm border-gray-300 rounded-md" type="text"
                            name="libelle" value="{{old('libelle')}}">
                            @error('libelle')
                                <div class="mt-1 text-red-500">{{ $message }}</div>
                            @enderror
                    </div>
                    <div class="py-2">
                        <label class="block mb-1" for="description">{{ __('lang.ref.description') }}</label>
                        <textarea name="description" id="description" cols="30"
                            rows="10">{{old('description')}}</textarea>
                            @error('description')
                                <div class="mt-1 text-red-500">{{ $message }}</div>
                            @enderror
                    </div>
                    <button
                        class="mt-5 rounded-md bg-green-500 text-white border border-black p-2 hover:bg-green-600 hover:text-white"
                        type="submit">{{ __('lang.ref.add') }}</button>
                </form>
            </div>
            <hr class="border border-black my-5">

            <div>
                <h2 class="text-black text-2xl font-bold">{{ __('lang.ref.list') }}</h2>




                <ul role="list" class="grid grid-cols-1 gap-6 sm:grid-cols-2 lg:grid-cols-3 mt-4">
                    @forelse ($referentiels as $referentiel)
                    <li class="col-span-1 bg-white rounded-lg shadow-xl divide-y divide-gray-200">
                        <div class="w-full flex items-center justify-between p-6 space-x-6">
                            <div class="flex-1 truncate">
                                <div class="flex items-center space-x-3">
                                    <h3 class="text-gray-900 text-sm font-medium truncate">
                                    {{ $referentiel->libelle}}</h3>
                                </div>
                                <p class="mt-1 text-gray-500 text-sm truncate">
                                {{ __('lang.ref.created_at') }} {{$referentiel->created_at->format('d/m/Y') }}</p>
                            </div>
                        </div>
                        <div>
                            <div class="-mt-px flex divide-x divide-gray-200">
                                <div class="w-0 flex-1 flex">
                                    <a href="{{route("referentiel.show",$referentiel)}}"
                                        class="relative -mr-px w-0 flex-1 inline-flex items-center justify-center py-4 text-sm text-gray-700 font-medium border border-transparent rounded-bl-lg hover:text-gray-500">

                                        <svg class="w-5 h-5 text-gray-400" viewBox="0 0 24 24" width="24" height="24" stroke="currentColor" stroke-width="2" fill="none" stroke-linecap="round" stroke-linejoin="round"><polyline points="9 18 15 12 9 6"></polyline></svg>
                                        <span class="ml-3">Accéder</span>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </li>
                    @empty
                    {{ __('lang.ref.empty') }}
                    @endforelse
                </ul>

            </div>
        </div>
    </div>
    <script>
        // In page pour le moment, à externaliser plus tard
        (() => {
            ClassicEditor.
            create(document.getElementById('description'), {
                    toolbar: ['bold', 'italic', 'bulletedList', 'numberedList', 'heading']
                })
                .catch(error => {
                    console.error(error);
                });
        })()

    </script>
</x-app-layout>

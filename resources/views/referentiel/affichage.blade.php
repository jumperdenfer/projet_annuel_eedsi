<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('lang.ref') }} {{$referentiel->libelle}}
        </h2>
    </x-slot>
    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8 bg-white shadow-lg p-5 rounded-lg">
            <div class="all-reset bg-white rounded-lg border-2 shadow-lg p-2 pl-4 mb-4">
                {!! $referentiel->description !!}
            </div>
            <div x-data="{form_open: false}">
                {{-- Button permettant de changer l'etat du formulaire d'edition des informations de base --}}
                <button class="rounded-md bg-green-500 text-white border border-black p-2 hover:bg-green-600 hover:text-white" @click="form_open = !form_open">{{ __('lang.ref.edit')}}</button>
                <div x-show="form_open" class="bg-white rounded-lg shadow-lg border-2 p-2 pl-4 py-4 mt-4">
                    <form action="{{route("referentiel.edit",[$referentiel->id])}}" method="post">
                        @csrf
                        <input type="hidden" name="id" value="{{$referentiel->id}}">
                        <div class="pt-2 pb-4">
                            <label class="block mb-2" for="libelle">{{__('lang.ref.libelle')}}</label>
                            <input id="libelle" class="shadow-sm block sm:text-sm border-gray-300 rounded-md" type="text" name="libelle" value="{{$referentiel->libelle}}">
                            @error('libelle')
                                <div class="mt-1 text-red-500">{{ $message }}</div>
                            @enderror
                        </div>
                        <div class="py-2 appearance-none">
                            <label class="block mb-2" for="description">{{__('lang.ref.description')}}</label>
                            <textarea name="description" id="description" cols="30" rows="10">{{$referentiel->description ?? ''}}</textarea>
                            @error('description')
                                <div class="mt-1 text-red-500">{{ $message }}</div>
                            @enderror
                        </div>
                        <x-button.submit :texte="__('Modifier le référentiel')"></x-button.submit>
                    </form>
                </div>
            </div>
            <hr class="border-black my-5">
            <div>
                {{-- Form pour ajouter une catégorie --}}
                <div x-data="{ajout_categorie_form: false}">
                    <button class="rounded bg-blue-700 text-white border border-black p-2 hover:bg-white hover:text-blue-700" 
                        @click="ajout_categorie_form = !ajout_categorie_form">
                        {{ __('lang.ref.add.category')}}
                    </button>
                    <div x-show="ajout_categorie_form" class="mt-6">
                        <form action="{{route('categorie.store')}}" method="POST">
                            @csrf
                            <input type="hidden" name="parentable_id" value="{{$referentiel->id}}">
                            <input type="hidden" name="parentable_type" value="referentiel">
                            <div>
                                <label class="block" for="libelle_categorie">{{__('lang.ref.lib.category')}}</label>
                                <input id="libelle_categorie" class="shadow-sm block sm:text-sm border-gray-300 rounded-md" type="text" name="libelle">
                                @error('libelle')
                                    <div class="mt-1 text-red-500">{{ $message }}</div>
                                @enderror
                            </div>
                            <div class="appearance-none">
                                <label for="description_categorie">{{__('lang.ref.lib.description')}}</label>
                                <textarea name="description" class="shadow-sm block sm:text-sm border-gray-300 rounded-md" id="description_categorie" cols="30" rows="10"></textarea>
                                @error('description')
                                    <div class="mt-1 text-red-500">{{ $message }}</div>
                                @enderror
                            </div>
                            <div>
                                <label class="block" for="coef_categorie">{{ __('lang.ref.category.coef')}}</label>
                                <input type="number" name="coef" class="shadow-sm block sm:text-sm border-gray-300 rounded-md" id="coef_categorie" min="0" max="10" value="1" step="0.01">
                                @error('coef')
                                    <div class="mt-1 text-red-500">{{ $message }}</div>
                                @enderror
                            </div>
                            <div>
                                <x-button.submit :texte="__('lang.ref.save.category')"></x-button.submit>
                            </div>
                        </form>
                    </div>
                </div>
                <hr class="border-black my-5">
            </div>
            <div class="mt-10" x-data="{categorie_show : false}">
                <h2 class="font-semibold text-xl text-gray-800 leading-tight mb-4">
                    {{ __('lang.ref.list.category') }}
                </h2>
                <div class="space-x-4">
                    {{-- Button Categorie --}}

                    @foreach ($referentiel->categories as $categorie) 
                        <div class="inline-block">
                            <button @click="categorie_show = {{$categorie->id}}"
                                class="rounded bg-blue-700 text-white border border-black p-2 hover:bg-white hover:text-blue-700" >
                                {{$categorie->libelle}}
                            </button>
                        </div>
                    @endforeach
                </div>
                {{-- Block des catégories --}}
                @forelse ($referentiel->categories as $categorie)
                    <x-referentiels.categorie class="bg-white mt-10 p-5" x-show="categorie_show  == {{$categorie->id}}" :categorie="$categorie"></x-referentiels.categorie>
                @empty
                    {{ __('lang.ref.category.empty')}}
                @endforelse
            </div>
           

        </div>
    </div>
    <script>
        // In page pour le moment, à externaliser plus tard
        (()=>{
            document.querySelectorAll('textarea').forEach(element=>{
                ClassicEditor.
                create(element,{
                    toolbar: ['bold', 'italic', 'bulletedList', 'numberedList', 'heading']
                })
                .catch( error => {
                    console.error( error );
                } );
            })
        })();
    </script>
</x-app-layout>

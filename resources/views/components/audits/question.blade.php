@props(['question', 'reponse'])
<form id="formulaire_question_{{$question->id}}" data-form="question" class="mb-8">
    <input type="hidden" name="question_id" value="{{$question->id}}">
    <div class="bg-white shadow overflow-hidden sm:rounded-lg">
        <div class="py-5 sm:px-6">
            <h3 class="text-lg leading-6 font-medium text-gray-900">
                {{$question->libelle}}
            </h3>
        </div>
        <div class="border-t border-gray-200 py-5 sm:px-6">
            <dl class="grid grid-cols-1 gap-x-4 gap-y-8 sm:grid-cols-2">
                <div class="sm:col-span-2">
                    <dt class="text-sm font-medium text-gray-500">
                        Description
                    </dt>
                    <dd class="mt-1 text-sm text-gray-900">
                        Fugiat ipsum ipsum deserunt culpa aute sint do nostrud anim incididunt cillum culpa consequat.
                        Excepteur qui ipsum aliquip consequat sint. Sit id mollit nulla mollit nostrud in ea officia
                        proident. Irure nostrud pariatur mollit ad adipisicing reprehenderit deserunt qui eu.
                    </dd>
                </div>
                <div class="sm:col-span-1">
                    <dt class="text-sm font-medium text-gray-500">
                        Lien externe
                    </dt>
                    <dd class="mt-1 text-sm text-gray-900">
                        {{$question->lien}}
                    </dd>
                </div>
                <div class="sm:col-span-2">
                    <dt class="text-sm font-medium text-gray-500">
                        Attachments
                    </dt>
                    <dd class="mt-1 text-sm text-gray-900">
                        <div class="mt-4">
                            <label class="block mb-2" for="question_lien_externe_{{$question->id}}">Note :</label>
                            <input type="number" class="shadow-sm block sm:text-sm border-gray-300 rounded-md w-full"
                                    name="note" min="0" max="1" value="{{optional($reponse)->note}}">
                        </div>
                        <div class="mt-6">
                            <label class="block mb-2" for="commentaire">Commentaire :</label>
                            <textarea name="commentaire" id="commentaire_{{$question->id}}" cols="30"
                                    rows="10">{!!optional($reponse)->commentaire!!}</textarea>
                        </div>
                    </dd>
                </div>

            </dl>
            <div class="flex items-center justify-end mt-4">
                <button type="submit"
                    class="inline-flex items-center px-4 py-2 border border-transparent text-base font-medium rounded-md shadow-sm text-white  bg-green-500 hover:bg-green-600 focus:outline-none">
                    {{__('Envoyer')}}
                </button>
            </div>
        </div>
    </div>
</form>

@props(['categorie', 'reponse'])
<div {{$attributes}}>
    @forelse($categorie->questions as $question)
        <x-audits.question :question="$question" :reponse="$reponse->where('question_id', $question->id)->first()"></x-audits.question>
    @empty
        <!-- <p>Aucune question</p> -->
    @endforelse
</div>

@props(['categorie'])
<div {{$attributes}}>
    <h3 class='text-lg font-bold'>{{$categorie->libelle}}</h3>
    <div class="all-reset text-sm">
        {!! $categorie->description !!}
    </div>
    <hr class="my-3">
    @foreach ($categorie->questions as $question)
        <x-rapport.question :question='$question' class="my-5"></x-rapport.question>
    @endforeach
</div>
@props(['nom','score'])
<div class="relative pt-1">
    @php 
        $roundedScore = round($score);
    @endphp 
    @if($score <= 25 && $score !== false)
    <div>{{$nom}}: {{__('Score:')}} {{$roundedScore}} %</div>
    <div class="overflow-hidden h-2 mb-4 text-xs flex rounded bg-red-200">
        <div style="width:{{$roundedScore}}%" class="shadow-none flex flex-col text-center whitespace-nowrap text-white justify-center bg-red-500"></div>
    </div> 
    @elseif($score >=75)
    <div>{{$nom}}: {{__('Score:')}} {{$roundedScore}} %</div>
    <div class="overflow-hidden h-2 mb-4 text-xs flex rounded bg-green-200">
        <div style="width:{{$roundedScore}}%" class="shadow-none flex flex-col text-center whitespace-nowrap text-white justify-center bg-green-500"></div>
    </div> 
    @elseif($score == false) 
    <div>{{$nom}}: {{__('Neutralisé')}}</div>
    <div class="overflow-hidden h-2 mb-4 text-xs flex rounded bg-gray-200">
        <div style="width:{{$roundedScore}}%" class="shadow-none flex flex-col text-center whitespace-nowrap text-white justify-center bg-gray-500"></div>
    </div> 
    @else 
    <div>{{$nom}}: {{__('Score:')}} {{$roundedScore}} %</div>
    <div class="overflow-hidden h-2 mb-4 text-xs flex rounded bg-indigo-200">
        <div style="width:{{$roundedScore}}%" class="shadow-none flex flex-col text-center whitespace-nowrap text-white justify-center bg-indigo-500"></div>
    </div> 
    @endif
</div>
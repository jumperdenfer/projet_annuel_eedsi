@props(['audit'])
<div {{$attributes}}>
    <h2 class="text-lg font-bold">{{__("Compte rendu de l'audit")}}</h2>
    <div>
        @foreach ($audit->referentiel->categories as $categorie)
            <x-rapport.barre_progression :nom="$categorie->libelle" :score="$categorie->score"></x-rapport.barre_progression>
        @endforeach
    </div>
    <div class="mt-10">
        <h2 class="text-lg font-bold">{{ __('Commentaire global:')}}</h2>
        <div class="all-reset">
            {!!$audit->commentaire!!}
        </div>
    </div>
</div>
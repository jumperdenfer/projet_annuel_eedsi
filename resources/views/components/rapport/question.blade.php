@props(['question'])
<div {{$attributes}}>
    <div class="p-3 border grid grid-cols-3 @if(isset($question->score) && $question->score !=1 ) bg-red-200 border-red-500 @elseif($question->score == 1) bg-green-200 border-green-500 @else bg-gray-200 border-gray-500  @endif">
        <div class="border-r-2 border-gray-900 px-2">
            <p class="text-xl font-bold"> {{$question->libelle}}</p>
            <div class="text-xs">
                {!! $question->description !!}
            </div>
            <a href="{{$question->lien}}" rel="nofollow noopeneer">{{__('Lien externe')}}</a>
        </div>
        <div class="border-r-2 border-gray-900 px-2">
            @if($question->score == 1)
                <p class='text-center text-xl font-bold'>{{__('Conforme')}}</p>
            @elseif(isset($question->score) && $question->score == 0)
                <p class='text-center text-xl font-bold'>{{__('Non conforme')}}</p>
            @else
                <p class='text-center text-xl font-bold'>{{__('Neutralisée')}}</p>
            @endif
        </div>
        <div class="px-2">
            <p>{{__('Commentaire:')}}</p>
            <div>
                {!! $question->commentaire !!}
            </div>
        </div>
    </div>
</div>
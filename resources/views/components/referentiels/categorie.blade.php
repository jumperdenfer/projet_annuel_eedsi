@props(['categorie'])
<div {{$attributes}}>
    <h2>{{__('lang.libelle')  }} {{$categorie->libelle}}</h2>
    <div class="all-reset">
    {{__('lang.category.description')  }} {!! $categorie->description !!}
    </div>
    <hr class="border-1 border-gray-600">
    <div x-data="{newquestion : false, editCategorie: false}">
        <div class="inline-block p-2">
            <button class="rounded bg-green-500 text-white border border-black p-2 hover:bg-green-400 hover:text-white" @click="newquestion = !newquestion; editCategorie = false">{{__('lang.add.question')}}</button>
            <button class="rounded bg-yellow-500 text-white border border-black p-2 hover:bg-yellow-400 hover:text-white" @click="newquestion = false; editCategorie = !editCategorie">{{ __('lang.edit.category')}}</button>
        </div>
        <div x-show="editCategorie">
            <form action="{{route('categorie.edit',$categorie->id)}}" method="POST">
                @csrf
                <div>
                    <label class="block" for="categorie_libelle_{{$categorie->id}}">{{__('lang.libelle')}}</label>
                    <input type="text" class="shadow-sm block sm:text-sm border-gray-300 rounded-md" name="libelle" value="{{$categorie->libelle}}" required>
                </div>
                <div>
                    <label class="block"  for="categorie_description_{{$categorie->id}}">{{__('lang.description')}}</label>
                    <textarea name="description" id="categorie_description_{{$categorie->id}}" cols="30" rows="10">{!! $categorie->description ?? null !!}</textarea>
                </div>
                <div>
                    <label class="block" for="categorie_coef_{{$categorie->coef}}">{{__('lang.coef')}}</label>
                    <input type="number" class="shadow-sm block sm:text-sm border-gray-300 rounded-md" name="coef" id="categorie_coef_{{$categorie->coef}}" min="0" max="10" value="{{$categorie->coef}}" step="0.01">
                </div>
                <div>
                    <x-button.submit :texte="__('lang.save.modif')"></x-button.submit>
                </div>
            </form>
            <form action="{{route('categorie.delete',$categorie->id)}}" method="POST">
                @csrf
                <x-button.delete :texte="__('lang.delete.category')"></x-button.delete>
            </form>
        </div>

        <div x-show="newquestion">
            <form action="{{route('question.store')}}" method="POST">
                @csrf 
                <input type="hidden" name="categorie" value="{{$categorie->id}}">
                <div>
                    <label class="block" for="new_question_libelle">{{ __('lang.libelle')}}</label>
                    <input type="text" class="shadow-sm block sm:text-sm border-gray-300 rounded-md" id="new_question_libelle" name="libelle" required>
                </div>
                <div>
                    <label class="block" for="new_question_description">{{ __('lang.description')}}</label>
                    <textarea name="description" id="new_question_description"></textarea>
                </div>
                <div>
                    <label class="block" for="new_question_coef">{{ __('lang.coef')}}</label>
                    <input type="number" class="shadow-sm block sm:text-sm border-gray-300 rounded-md" step="0.1" name="coef" id="new_question_coef" value="1" required>
                </div>
                <div>
                    <label class="block" for="new_question_lien_externe">{{__('lang.external.link')}}</label>
                    <input type="text" class="shadow-sm block sm:text-sm border-gray-300 rounded-md" name="lien" id="new_question_lien_externe">
                </div>
                <x-button.submit :texte="__('lang.save.question')"></x-button.submit>
            </form>
        </div>
    </div>
    <hr class="mb-4 border border-gray-400">
    <div>
        @forelse ($categorie->questions as $question)
            <x-referentiels.question :question="$question"></x-referentiels.question>
            <hr>
        @empty
            <p>{{__('lang.question.empty')}}</p>
        @endforelse
    </div>
</div>
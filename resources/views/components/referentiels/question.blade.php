@props(['question'])
<div class="grid grid-cols-3 m-3">
    <div>
        {{__('lang.question.libelle')}} <h5 class="text-lg font-bold">{{$question->libelle}}</h5>
        <p>{{__('lang.coef')}} {{$question->coef}}</p>
    </div>
    <div class="all-reset">
        <p>{{__('lang.description')}} {!! $question->description !!}</p>
        @if($question->lien)
        {{__('lang.external.link')}} <a href="{{$question->lien}}">{{$question->lien}}</a>
        @endif
    </div>
    <div x-data="{formulaire : false}">
        <button @click="formulaire = !formulaire"
            class="inline-flex items-center px-3 py-2 border border-transparent shadow-sm text-sm leading-4 font-medium rounded-md text-white bg-blue-500 focus:outline-none">
            {{__('lang.modif')}}
            <svg class="ml-2 h-4 w-4" viewBox="0 0 24 24" width="24" height="24" stroke="#fff" stroke-width="2" fill="none"
                stroke-linecap="round" stroke-linejoin="round">
                <polyline points="6 9 12 15 18 9"></polyline>
            </svg>
        </button>
        <div x-show="formulaire" class="mt-4">
            <form action="{{route('question.update',$question->id)}}" method="POST">
                @csrf
                <div>
                    <label class="block" for="question_libelle_{{$question->id}}">{{ __('lang.libelle')}}</label>
                    <input type="text" class="shadow-sm block sm:text-sm border-gray-300 rounded-md" id="question_libelle_{{$question->id}}" name="libelle"
                        value="{{$question->libelle}}" required>
                </div>
                <div class="mt-4">
                    <label class="block" for="question_description_{{$question->id}}">{{ __('lang.description')}}</label>
                    <textarea name="description"
                        id="question_description_{{$question->id}}">{{$question->description ?? ''}}</textarea>
                </div>
                <div class="mt-4">
                    <label class="block" for="question_coef_{{$question->id}}">{{ __('lang.coef')}}</label>
                    <input type="number" class="shadow-sm block sm:text-sm border-gray-300 rounded-md" step="0.1" name="coef" id="question_coef_{{$question->id}}"
                        value="{{$question->coef ?? 1}}" required>
                </div>
                <div class="mt-4">
                    <label class="block" for="question_lien_externe_{{$question->id}}">{{__('lang.external.link')}}</label>
                    <input type="text" class="shadow-sm block sm:text-sm border-gray-300 rounded-md" name="lien" value="{{$question->lien}}" id="question_lien_externe_{{$question->id}}">
                </div>
                <x-button.submit :texte="__('lang.modif.question')"></x-button.submit>
            </form>
            <form action="{{route('question.delete',$question->id)}}" method="POST">
                @csrf
                <input type="hidden" name="delete" value="true">
                <x-button.delete :texte="__('lang.delete.question')"></x-button.delete>
            </form>
        </div>
    </div>
</div>

@props(['texte'])
<div>
    <button class="mt-5 rounded-md bg-green-500 text-white border border-black p-2 hover:bg-green-600 hover:text-white" type="submit">
        {{ $texte }}
    </button>
</div>
<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ $referentielFull->libelle }} - {{ $auditSelected->predicted_at->format('d/m/Y') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <input type="hidden" name="audit_id" id="audit" value="{{$auditSelected->id}}">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8 bg-white shadow-lg rounded-lg py-4">
            <div class="mt-10" x-data="{category_show : false}">
                <h2 class="font-semibold text-xl text-gray-800 leading-tight mb-4">
                    {{ __('Liste des questions par catégorie : ') }}
                </h2>



                <ul role="list" class="mt-3 grid grid-cols-1 gap-5 sm:gap-6 sm:grid-cols-2 lg:grid-cols-4 mb-4">

                    @foreach ($referentielFull->categories as $categorie)
                    <button @click="category_show  = {{$categorie->id}}">
                        <li class="col-span-1 flex shadow-sm rounded-md">
                            <div
                                class="flex-1 flex items-center justify-between border border-gray-200 bg-blue-500 rounded-md truncate">
                                <div class="flex-1 px-4 py-2 text-sm truncate">
                                    <span class="text-white font-bold">{{$categorie->libelle}}</span>
                                </div>
                                <div class="flex-shrink-0 pr-2">
                                    <div
                                        class="w-8 h-8 inline-flex items-center justify-center text-white focus:outline-none">

                                        <svg class="h-5 w-5" viewBox="0 0 24 24" width="24" height="24"
                                            stroke="currentColor" stroke-width="2" fill="none" stroke-linecap="round"
                                            stroke-linejoin="round">
                                            <polyline points="6 9 12 15 18 9"></polyline>
                                        </svg>
                                    </div>
                                </div>
                            </div>
                        </li>
                    </button>
                    @endforeach

                </ul>

                {{-- Block des catégories --}}
                @forelse ($referentielFull->categories as $categorie)
                <x-audits.category :categorie='$categorie' :reponse='$auditSelected->reponses'
                    x-show="category_show  == {{$categorie->id}}" :audit='$auditSelected->id'></x-audits.category>
                @empty
                {{ __('Aucune catégorie')}}
                @endforelse
            </div>
            <div>
                <form action="{{route("audit.commentaire", [$auditSelected->id]) }}" method="post">
                    @csrf
                    <div class="py-2">
                        <label class="block mb-1" for="commentaire">{{ __('lang.audit.commentaire') }}</label>
                        <textarea name="commentaire" id="commentaire" cols="30"
                            rows="10">{{ $auditSelected->commentaire }}</textarea>
                        @error('commentaire')
                        <div class="mt-1 text-red-500">{{ $message }}</div>
                        @enderror
                    </div>
                    <button
                        class="mt-5 rounded-md bg-green-500 text-white border border-black p-2 hover:bg-green-600 hover:text-white"
                        type="submit">{{ __('lang.audit.send.audit') }}</button>
                </form>
            </div>
        </div>
    </div>
    <script>
        // In page pour le moment, à externaliser plus tard
        (() => {
            document.querySelectorAll('textarea').forEach(element => {
                ClassicEditor.
                create(element, {
                        toolbar: ['bold', 'italic', 'bulletedList', 'numberedList', 'heading']
                    })
                    .catch(error => {
                        console.error(error);
                    });
            })
        })();
        //Gestion des formulaires de question
        (() => {
            document.querySelectorAll('[data-form=question]').forEach((form) => {
                form.addEventListener('submit', (e) => {
                    e.preventDefault();
                    let formulaire = new FormData(form);
                    let audit_id = document.getElementById('audit').value;
                    formulaire.append('audit_id', audit_id);
                    axios.post('/users/reponses/store', formulaire, {
                            headers: {
                                "Content-Type": 'multipart/form-data'
                            }
                        })
                        .then((res) => {
                            //Mettre ici la popup success si le code de res.status est égale à 200
                        })
                })
            })
        })();

    </script>
</x-app-layout>

<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Gestion Audit') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8 bg-white shadow-lg rounded-lg py-4">
            {{-- Rendre cette partie en " caché "  --}}
            <div>
                @if (count($errors) > 0)
                <div class="bg-red-100 border-t-4 border-red-500 rounded-md text-red-900 px-4 py-3 shadow-md mb-4"
                    role="alert">
                    <div class="flex">
                        <div class="py-1"><svg class="fill-current h-6 w-6 text-red-500 mr-4"
                                xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20">
                                <path
                                    d="M2.93 17.07A10 10 0 1 1 17.07 2.93 10 10 0 0 1 2.93 17.07zm12.73-1.41A8 8 0 1 0 4.34 4.34a8 8 0 0 0 11.32 11.32zM9 11V9h2v6H9v-4zm0-6h2v2H9V5z" />
                            </svg></div>
                        <div>
                            <p class="font-bold">Opps!</p>
                            <p class="text-sm">Something went wrong, please check below errors.</p>
                        </div>
                    </div>
                </div>
                @endif

                <form action="{{route('audit.store')}}" method="POST">
                    @csrf
                    <div class="overflow-hidden sm:rounded-md">
                        <h2 class="text-2xl font-bold">{{ __('Nouvel audit')}} :</h2>
                        <div class="px-4 py-5 sm:p-6">
                            <div class="grid grid-cols-6 gap-6 mt-6">

                                <div class="col-span-6 sm:col-span-6 lg:col-span-2">
                                    <label for="email"
                                        class="block text-sm font-medium text-gray-700">{{__('Email')}}</label>
                                    <input type="text" name="email" id="email" value="{{old('email')}}"
                                        class="mt-1 focus:ring-indigo-500 focus:border-indigo-500 block w-full shadow-sm sm:text-sm border-gray-300 rounded-md">
                                    @error('email')
                                    <div class="mt-1 text-red-500">{{ $message }}</div>
                                    @enderror
                                </div>

                                <div class="col-span-6 sm:col-span-3 lg:col-span-2">
                                    <label for="predicted_at"
                                        class="block text-sm font-medium text-gray-700">{{__('Date')}}</label>
                                    <input type="date" name="predicted_at" id="predicted_at"
                                        class="mt-1 focus:ring-indigo-500 focus:border-indigo-500 block w-full shadow-sm sm:text-sm border-gray-300 rounded-md">
                                    @error('predicted_at')
                                    <div class="mt-1 text-red-500">{{ $message }}</div>
                                    @enderror
                                </div>

                                <div class="col-span-6 sm:col-span-3 lg:col-span-2">
                                    <label for="referentiel_id"
                                        class="block text-sm font-medium text-gray-700">{{__('Liste des référentiels')}}</label>
                                    <select
                                        class="mt-1 focus:ring-indigo-500 focus:border-indigo-500 block w-full shadow-sm sm:text-sm border-gray-300 rounded-md"
                                        name="referentiel_id">
                                        @foreach ($referentiels as $referentiel)
                                        <option value="{{ $referentiel->id}}">{{ $referentiel->libelle}}</option>
                                        @endforeach
                                    </select>
                                    @error('referentiel_id')
                                    <div class="mt-1 text-red-500">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>
                        </div>
                        <div class="px-4 py-3 text-right sm:px-6">
                            <button type="submit"
                                class="inline-flex justify-center py-2 px-4 border border-transparent shadow-sm text-sm font-medium rounded-md text-white bg-green-500 hover:bg-green-600 focus:outline-none">
                                {{__('Ajouter nouveau référentiel')}}
                            </button>
                        </div>
                    </div>
                </form>
            </div>
            <hr class="border border-black my-5">

            <div>
                <h2 class="text-black text-2xl font-bold">{{__('Liste des audits')}} :</h2>


                <ul role="list" class="grid grid-cols-1 gap-6 sm:grid-cols-2 lg:grid-cols-3 mt-4">
                    @forelse ($audits as $audit)
                    <li class="col-span-1 bg-white rounded-lg shadow-xl divide-y divide-gray-200">
                        <div class="w-full flex items-center justify-between p-6 space-x-6">
                            <div class="flex-1 truncate">
                                <div class="flex items-center space-x-3">
                                    <h3 class="text-gray-900 text-sm font-medium truncate">
                                        {{ $audit->referentiel->libelle }}</h3>
                                </div>
                                <p class="mt-1 text-gray-500 text-sm truncate">
                                    {{$audit->predicted_at->format('d/m/Y') }}</p>
                            </div>
                        </div>
                        <div>
                            <div class="-mt-px flex divide-x divide-gray-200">
                                <div class="w-0 flex-1 flex">
                                    <a href="{{route("audit.show",$audit)}}"
                                        class="relative -mr-px w-0 flex-1 inline-flex items-center justify-center py-4 text-sm text-gray-700 font-medium border border-transparent rounded-bl-lg hover:text-gray-500">

                                        <svg class="w-5 h-5 text-gray-400" viewBox="0 0 24 24" width="24" height="24" stroke="currentColor" stroke-width="2" fill="none" stroke-linecap="round" stroke-linejoin="round"><polyline points="9 18 15 12 9 6"></polyline></svg>
                                        <span class="ml-3">Accéder</span>
                                    </a>
                                </div>
                                <div class="-ml-px w-0 flex-1 flex">
                                    <a href="{{ $audit->signedLink }}"
                                        class="relative w-0 flex-1 inline-flex items-center justify-center py-4 text-sm text-gray-700 font-medium border border-transparent rounded-br-lg hover:text-gray-500">
                                        <svg class="w-5 h-5 text-gray-400" viewBox="0 0 24 24" width="24" height="24" stroke="currentColor"
                                            stroke-width="2" fill="none" stroke-linecap="round" stroke-linejoin="round">
                                            <path d="M14 2H6a2 2 0 0 0-2 2v16a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V8z"></path>
                                            <polyline points="14 2 14 8 20 8"></polyline>
                                            <line x1="16" y1="13" x2="8" y2="13"></line>
                                            <line x1="16" y1="17" x2="8" y2="17"></line>
                                            <polyline points="10 9 9 9 8 9"></polyline>
                                        </svg>
                                        <span class="ml-3">Rapport</span>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </li>
                    @empty
                    {{ __('Aucun audit existant')}}
                    @endforelse
                </ul>

            </div>

        </div>
    </div>
</x-app-layout>

<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Statistique') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <ul role="list" class="grid grid-cols-1 gap-6 sm:grid-cols-2 lg:grid-cols-3 mt-4">
                @forelse ($referentiels as $referentiel)
                <li class="col-span-1 bg-white rounded-lg shadow-xl divide-y divide-gray-200">
                    <div class="w-full flex items-center justify-between p-6 space-x-6">
                        <div class="flex-1 truncate">
                            <div class="flex items-center space-x-3">
                                <h3 class="text-gray-900 text-sm font-medium truncate">
                                {{ $referentiel->libelle}}</h3>
                            </div>
                            <p class="mt-1 text-gray-500 text-sm truncate">
                                {{ __('lang.ref.created_at') }} {{$referentiel->created_at->format('d/m/Y') }}
                            </p>
                            <p>
                                {{__('Score:')}} {{$referentiel->finalScore ? round($referentiel->finalScore,2)."%" : __('Aucun  audit réaliser')  }}
                            </p>
                        </div>
                    </div>
                    <div>
                        <div class="-mt-px flex divide-x divide-gray-200">
                            <div class="w-0 flex-1 flex">
                                <a href="{{route("statistique.show",$referentiel)}}"
                                    class="relative -mr-px w-0 flex-1 inline-flex items-center justify-center py-4 text-sm text-gray-700 font-medium border border-transparent rounded-bl-lg hover:text-gray-500">

                                    <svg class="w-5 h-5 text-gray-400" viewBox="0 0 24 24" width="24" height="24" stroke="currentColor" stroke-width="2" fill="none" stroke-linecap="round" stroke-linejoin="round"><polyline points="9 18 15 12 9 6"></polyline></svg>
                                    <span class="ml-3">Accéder</span>
                                </a>
                            </div>
                        </div>
                    </div>
                </li>
                @empty
                {{ __('lang.ref.empty') }}
                @endforelse
        </div>
    </div>
</x-app-layout>

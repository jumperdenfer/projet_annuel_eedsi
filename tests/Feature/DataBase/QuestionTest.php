<?php

namespace Tests\Feature\DataBase;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;


use App\Models\User;
use App\Models\Referentiel;
use App\Models\Categorie;
use App\Models\Question;

class QuestionTest extends TestCase{
    /**
     * Test to create a question 
     */
    public function test_create_question(){
        $referentiel = Referentiel::factory()->for(User::Factory()->create())->create();

        $categorie = Categorie::factory()
        ->for($referentiel,'parentable')
        ->create();

        $question = Question::factory()
        ->for($categorie)
        ->create();
        $this->assertModelExists($question);
    }


    /**
     * Test to retrive all question for a categorie
     */
    public function test_to_retrieve(){
        $referentiel = Referentiel::factory()->for(User::Factory()->create())->create();

        $categorie = Categorie::factory()
        ->for($referentiel,'parentable')
        ->create();


        $question = Question::factory()
        ->for($categorie)
        ->create();

        $idFromCategorie = $categorie->questions->first()->id;
        $this->assertSame($question->id,$idFromCategorie);

    }

}
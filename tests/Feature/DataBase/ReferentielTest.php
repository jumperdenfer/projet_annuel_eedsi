<?php

namespace Tests\Feature\DataBase;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;


use App\Models\User;
use App\Models\Referentiel;

/**
 * Class permettant de vérifier que le model referentiel fonctionne correctement
 */
class ReferentielTest extends TestCase
{
    use RefreshDatabase;
    /**
     * test to create a referentiel for a specific user
     *
     * @return void
     */
    public function test_create_a_referentiel()
    {
        // Create an user
        $user = User::factory()->create();
        //verify if user exists 
        $this->assertModelExists($user);
        // create a referentiel for the previous user
        $referentiel = Referentiel::factory()
        ->for($user)
        ->create();
        //Verify if referentiel exists
        $this->assertModelExists($referentiel);
    }

    /**
     *  test for verify if referentiel->user->id is the same of the creator
     */
    public function test_get_user_for_a_referentiel(){
        //Create a new user
        $user = User::factory()->create();
        $this->assertModelExists($user);
        //Create a new Referentiel for the previous user
        $referentiel = Referentiel::factory()
        ->for($user)
        ->create();
        //Verify if id of user is the same of referentiel user id
        $this->assertSame($user->id,$referentiel->user->id);
    }

    /**
     * Test for verify if we can get all referentiel for a specific user
     */
    public function test_get_referentiel_from_user(){
        $user = User::factory()->create();
        $this->assertModelExists($user);

        Referentiel::factory()->count(10)->for($user)->create();
        
        $userReferentielCount = $user->referentiels->count();
        $this->assertSame($userReferentielCount,10);

    }

}

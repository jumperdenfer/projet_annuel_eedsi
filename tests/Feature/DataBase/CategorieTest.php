<?php

namespace Tests\Feature\DataBase;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;


use App\Models\User;
use App\Models\Referentiel;
use App\Models\Categorie;

class CategorieTest extends TestCase{
    use RefreshDataBase; 

    /**
     * Test to create a new categorie for a referentiel
     */
    public function test_create_categorie(){
        $referentiel = Referentiel::factory()->for(User::Factory()->create())->create();
        $this->assertModelExists($referentiel);

        $categorie = Categorie::factory()
        ->for(
            $referentiel,'parentable'
        )
        ->create();
        $this->assertModelExists($categorie);
    }


    /**
     * Test to create a new "sub-categorie" for a categorie ( sub-category)
     */
    public function test_create_categorie_for_categorie(){
        $referentiel = Referentiel::factory()->for(User::Factory()->create())->create();
        $categorie = Categorie::factory()
        ->for(
            $referentiel,'parentable'
        )
        ->create();

        $subCategorie = Categorie::factory()
        ->for(
            $categorie,'parentable'
        )
        ->create();
        $this->assertModelExists($subCategorie);

    }

    /**
     * Test to retrieve a categorie from a referentiel 
     */
    public function test_to_retrieve_categorie_from_referentiel(){
        $referentiel = Referentiel::factory()->for(User::Factory()->create())->create();
        $categorie = Categorie::factory()
        ->for(
            $referentiel,'parentable'
        )
        ->create();

        $referentielCategorie = $referentiel->categories->first();
        $this->assertSame($categorie->id,$referentielCategorie->id);
    }

    /**
     * Test to retrieve a sub categorie from a categorie
     */
    public function test_to_retrieve_subcategorie_from_categorie(){
        $referentiel = Referentiel::factory()->for(User::Factory()->create())->create();
        $categorie = Categorie::factory()
        ->for(
            $referentiel,'parentable'
        )
        ->create();

        $subCategorie = Categorie::factory()
        ->for(
            $categorie,'parentable'
        )
        ->create();

        $firstSubId = $categorie->categories->first()->id;
        $this->assertSame($firstSubId, $subCategorie->id);
    }
}
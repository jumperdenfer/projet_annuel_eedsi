<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateQuestionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('questions', function (Blueprint $table) {
            $table->id();
            //Nom de la question
            $table->string('libelle');
            // Description en WYSISWG de a question
            $table->text('description')->nullable();
            //Coefficient utilisable pour les calculs
            $table->float('coef',4,2)->default(1);
            //Lien vers un document externe au logiciel
            $table->string('lien')->nullable();
            //Id de la catégorie / sous-catégorie parent
            $table->foreignId('categorie_id');
            $table->softDeletes('deleted_at',0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('questions');
    }
}

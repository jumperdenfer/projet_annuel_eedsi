<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAuditsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('audits', function (Blueprint $table) {
            $table->id();
            // Email de la personnes qui doit recevoir l'audit
            $table->string('email');
            // Date prévisionnel de réalisations de l'audit
            $table->date('predicted_at');
            $table->date('commentaire')->nullable();
            // Score final de l'audit (Seul score enregistré en BDD)
            $table->float('score',4,2)->nullable();
            //Id du référentiel d'origine
            $table->foreignId('referentiel_id');
            $table->softDeletes('deleted_at',0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('audits');
    }
}

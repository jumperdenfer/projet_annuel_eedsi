<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('categories', function (Blueprint $table) {
            $table->id();
            //Libelle de la catégorie
            $table->string('libelle');
            //Description en WYSIWYG
            $table->text('description')->nullable();
            $table->float('coef',4,2)->default(1);
            // Permet d'avoir de définir des enfants interne à la catégorie
            $table->morphs('parentable');
            $table->softDeletes('deleted_at',0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('categories');
    }
}

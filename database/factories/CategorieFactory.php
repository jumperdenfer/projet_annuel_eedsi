<?php

namespace Database\Factories;

use App\Models\Categorie;
use App\Models\Referentiel;
use Illuminate\Database\Eloquent\Factories\Factory;

class CategorieFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Categorie::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {

        return [
            'libelle' => $this->faker->company(),
            'description' => $this->faker->randomHtml(1,2),
            'coef' => $this->faker->randomFloat(2,0,2),
        ];
    }
}

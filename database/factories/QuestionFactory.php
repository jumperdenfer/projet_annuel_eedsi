<?php

namespace Database\Factories;

use App\Models\Question;
use Illuminate\Database\Eloquent\Factories\Factory;

class QuestionFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Question::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'libelle' => $this->faker->company(),
            'description' => $this->faker->randomHtml(2,3),
            'coef' => $this->faker->randomFloat(2,0,2),
            'lien' => $this->faker->url(),
        ];
    }
}

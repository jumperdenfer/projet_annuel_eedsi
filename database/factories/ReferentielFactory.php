<?php

namespace Database\Factories;

use App\Models\Referentiel;
use App\Models\User;

use Illuminate\Database\Eloquent\Factories\Factory;

class ReferentielFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Referentiel::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            "libelle" => $this->faker->company(),
            "description" => $this->faker->catchPhrase(),
            "user_id" => User::pluck('id') ?? User::factory(), 
        ];
    }
}

<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ReponseRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'note' => 'required',
            'commentaire' => 'nullable',
            'audit_id' => 'required',
            'question_id' => 'required'
        ];
    }
}

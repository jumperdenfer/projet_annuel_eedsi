<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Services\ReponseService;
use App\Http\Requests\ReponseRequest;


class ReponseController extends Controller
{

    public $reponseGestionService;

    public function __construct(ReponseService $reponseGestionService){
        $this->reponseGestionService = $reponseGestionService;
    }
   
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ReponseRequest $request)
    {
        try{
            $id = $this->reponseGestionService->save($request->all());
            return response()->json(['id_question' => $id],200);
        }catch(\Exception $e){
            return response()->json(['id_question'=> false],422);
        }
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ReponseRequest $request, $id)
    {
        $this->reponseGestionService->update($request->all(),$id);
        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->reponseGestionService->delete($id);
        return back();
    }
}

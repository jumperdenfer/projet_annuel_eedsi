<?php
namespace App\Http\Controllers;



use Illuminate\Http\Request;
use App\Services\StatistiqueService;

class StatistiqueController extends Controller{

    public $statistiqueService;
    //Enable statistique Service provider
    public function __construct(StatistiqueService $statistiqueService){
        $this->statistiqueService = $statistiqueService;
    }
    /**
     * Index
     * Display all referentiels with global score
     */
    public function index(){
        $referentiels = $this->statistiqueService->getAll();
        return view('statistiques.accueil',compact('referentiels'));
    }

    public function show($id) {
        $referentiel = $this->statistiqueService->getOneById($id);
        return view('statistiques.show', compact('referentiel'));
    }

}
<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
//Services
use App\Services\ReferentielUserService;
use App\Services\ReferentielGlobalService;

//Request
use App\Http\Requests\ReferentielInitRequest;
use App\Http\Requests\ReferentielEditRequest;

//Model
use App\Models\Referentiel;


class ReferentielController extends Controller
{
    /**
     * Display a listing of the referentiel for the user
     *
     * @return \Illuminate\Http\Response
     */
    public function index(ReferentielUserService $referentielUserService){
        $referentiels = $referentielUserService->getAllForAuth();
        return view('referentiel.gestion_referentiel',compact('referentiels'));
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ReferentielInitRequest $request,ReferentielUserService $referentielUserService)
    {
        //Enregistre et récupère le référentiel
        $referentiel = $referentielUserService->saveReferentiel($request->all());
        return redirect()->route('referentiel.show',compact('referentiel'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Referentiel $referentiel,ReferentielGlobalService $referentielGlobalService)
    {
        $referentiel = $referentielGlobalService->loadFull($referentiel);
        return view('referentiel.affichage',compact('referentiel'));
    }

  
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,ReferentielUserService $referentielUserService)
    {
        $referentiel = $referentielUserService->updateReferentiel($request->all());
        return redirect()->route('referentiel.show',compact('referentiel'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

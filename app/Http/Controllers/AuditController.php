<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;

use App\Services\AuditUserService;
use App\Services\AuditGlobalService;

//Request
use App\Http\Requests\AuditInitRequest;

//Model
use App\Models\Audit;
use App\Services\ReferentielGlobalService;
use App\Services\ReferentielUserService;

class AuditController extends Controller
{
    /**
     * Display a listing of the referentiel for the user
     *
     * @return \Illuminate\Http\Response
     */

    public function index(AuditUserService $auditUserService, ReferentielUserService $referentielUserService){
        $audits = $auditUserService->getAllForAuth();
        $referentiels = $referentielUserService->getAllForAuth();
        return view('audits.index',compact('audits','referentiels'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function store(AuditInitRequest $request,AuditUserService $auditUserService)
    {
        //Enregistre et récupère le référentiel
        $auditUserService->saveAudit($request->all());
        return back();
    }

        /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function commentaire(Request $request, AuditUserService $auditUserService)
    {
        $auditUserService->addCommentaire($request->all(), $request->audit);
        return redirect()->route('audit.show', $request->audit);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function show(
        Audit $audit,
        AuditGlobalService $auditGlobalService,
        ReferentielUserService $referentielUserService,
        ReferentielGlobalService $referentielGlobalService)
    {
        $auditSelected = $auditGlobalService->loadReponse($audit);
        $referentielFull = $referentielGlobalService->loadFull($referentielUserService->getOneById($auditSelected->referentiel_id));
        return view('audits.show',compact('auditSelected', 'referentielFull'));
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

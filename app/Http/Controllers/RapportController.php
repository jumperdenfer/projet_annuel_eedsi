<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Services\RapportGestionService;

class RapportController extends Controller
{
    public $rapportGestionService;

    public function __construct(RapportGestionService $rapportGestionService){
        $this->rapportGestionService = $rapportGestionService;
    }

    /**
     * Permet l'affichage d'un rapport
     */
    public function index(int $audit){
        $auditFull = $this->rapportGestionService->loadScore($audit);
        return view('rapport.show',compact('auditFull'));
    }
}
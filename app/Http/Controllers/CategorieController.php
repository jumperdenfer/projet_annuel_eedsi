<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

//Service
use App\Services\CategorieGestionService;

//Request 
use App\Http\Requests\CategorieRequest;

class CategorieController extends Controller
{
    
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CategorieRequest $request,CategorieGestionService $categorieService)
    {
        $categorieService->save($request->all());
        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(CategorieRequest $request, CategorieGestionService $categorieService, $id)
    {
        $categorieService->update($request->all(),$id);
        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(CategorieGestionService $categorieService, $id)
    {
        $categorieService->delete($id);
        return back();
    }
}

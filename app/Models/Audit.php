<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Audit extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var string[]
     */
    protected $fillable = [
        'email',
        'predicted_at',
        'commentaire',
        'referentiel_id'
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array
     */
    protected $casts = [
        'predicted_at' => 'datetime',
    ];

    /**
     * Relation One To Many with User
    */
    public function referentiel(){
        return $this->belongsTo(Referentiel::class);
    }

    public function reponses() {
        return $this->hasMany(Reponse::class);
    }
}

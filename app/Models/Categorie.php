<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;

class Categorie extends Model
{
    use HasFactory;
    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var string[]
     */
    protected $fillable = [
        'libelle',
        'description',
        'coef'
    ];

    /**
     * Define the polymorphic relationships with Referentiel Or SubCategory
     */
    public function parentable(){
        return $this->morphTo();
    }

    /**
     * Relation with subcategorie 
     */
    public function categories(){
        return $this->morphMany(Categorie::class,'parentable');
    }

    /**
     * relationships with Question
     */
    public function questions(){
        return $this->hasMany(Question::class);
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Referentiel extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var string[]
     */
    protected $fillable = [
        'libelle',
        'description',
        'user_id'
    ];

    /**
     * Relation One To Many with User
     */
    public function user(){
        return $this->belongsTo(User::class);
    }

    /**
     * Relation On to Many (Morph) with Categorie
     */
    public function categories(){
        return $this->morphMany(Categorie::class,'parentable');
    }

    /**
     * Relation has many with Audit
     */
    public function audits(){
        return $this->hasMany(Audit::class);
    }
}

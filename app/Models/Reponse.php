<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Reponse extends Model
{
    use HasFactory;

    protected $fillable = [
        'note',
        'commentaire',
        'audit_id',
        'question_id'
    ];

    public function audit(){
        return $this->belongsTo(Audit::class);
    }

}

<?php 

namespace App\Services;

use Illuminate\Support\Facades\Auth;

use App\Models\Audit;
use Illuminate\Support\Facades\URL;

class AuditUserService {

    /**
     * get all referentiel for the Auth User
     * @return array $referentiel
     */
    public function getAllForAuth(){

        $audits = Audit::whereHas('referentiel',function($q){
           $q->where('user_id',Auth::User()->id); 
         })->get();

         $audits->map(function($audit) {
             $audit->signedLink = URL::signedRoute('audit.rapport', compact('audit'));
         });

         return $audits;
    }
    
    /**
     * Create and save a referentiel
     * @param array $request
     * @return Audit;
     */
    public function saveAudit($request){
        return Audit::create([
            'email' => $request['email'],
            'predicted_at' => $request['predicted_at'],
            'referentiel_id' => $request['referentiel_id']
        ]);
    }

    /**
     * Update a existing referentiel 
     * @param array $referentiel
     * @return Audit
     */
    public function updateAudit($request){
        $audit = Audit::where('id',$request['id'])
        ->first();

        $audit->fill([
            'email' => $request['email'],
            'predicted_at' => $request['predicted_at'],
            'referentiel_id' => $request['referentiel_id']
        ])->save();
        return $audit;
    }

    /**
     * Create and save a commentaire
     * @param array $request
     * @return Audit;
     */
    public function addCommentaire($request, $id){
        $audit = Audit::where('id',$id)->first();

        $audit->fill([
            'commentaire' => $request['commentaire'],
        ])->save();
    }
}
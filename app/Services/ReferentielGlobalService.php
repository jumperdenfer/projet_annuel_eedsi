<?php 

namespace App\Services;

use Illuminate\Support\Facades\Auth;

use App\Models\Referentiel;


class ReferentielGlobalService{

    public function loadFull(Referentiel $referentiel){
        return $referentiel->load([
            'categories' => function($categorieQuery){
                $categorieQuery->with([
                    'questions',
                    'categories' => function($subCategorieQuery){
                        $subCategorieQuery->with('questions');
                    }
                ]);
            }
        ]);
    }
}
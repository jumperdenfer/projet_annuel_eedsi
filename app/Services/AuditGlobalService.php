<?php 

namespace App\Services;

use Illuminate\Support\Facades\Auth;

use App\Models\Audit;
use App\Models\Referentiel;

class AuditGlobalService{

    // public function getOneAuditWithResponse($id){
    //     return Audit::where('id', $id)
    //     ->with('reponse')
    //     ->first();
    // }

    public function loadReponse($audit) {
        return $audit->load('reponses');
    }

}
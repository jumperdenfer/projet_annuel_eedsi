<?php 

namespace App\Services;

use Illuminate\Support\Facades\Auth;

use App\Models\Categorie;
use App\Models\Referentiel;


class CategorieGestionService{

    /**
     * Save a category into the parent model
     * @param array $request 
     * @param Model $parent
     * @return object
     */
    public function save($request){
        $parent = $this->factoryLoadParent($request['parentable_id'],$request['parentable_type']);
        $categorie = Categorie::make([
            'libelle' => $request['libelle'],
            'description' => $request['description'],
            'coef' => $request['coef']
        ]);
        return $parent->categories()->save($categorie);
    }

    /**
     * Update a categorie if the user is allowed
     * @param array $request 
     * @param int $id 
     * @return object
     */
    public function update($request,$id){
        $categorie = Categorie::where('id',$id)
        ->firstOrFail();

        $categorie->fill([
            'libelle' => $request['libelle'],
            'description' => $request['description'],
            'coef' => $request['coef'],
        ])->save();
        return $categorie;
    }

    public function delete($id){
        return Categorie::where('id',$id)->delete();
    }

    /**
     * Get the corresponding Model
     * @param int $id
     * @param string $type
     * @return Model $parent
     */
    private function factoryLoadParent($id,$type){
        $parent = null;
        switch($type){
            case 'referentiel':
                $parent = Referentiel::where('id',$id)->firstOrFail();
                break;
            case 'categorie':
                $parent = Categorie::where('id',$id)->firstOrFail();
                break;
            default:
                break; 
        }
        return $parent;
           
    }

}
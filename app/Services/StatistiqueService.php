<?php 
namespace App\Services;

use App\Models\Referentiel;
use App\Services\ReferentielUserService;
use App\Services\RapportGestionService;
use Illuminate\Support\Facades\Auth;

class StatistiqueService{

    /**
     * GetAll
     * Return an array of Referentiel with all Diagnostic score Calculated
     */
    public function getAll(){
        $referentiels = (new ReferentielUserService)->getAllForAuth();
       
        return $referentiels->map(function($referentiel){
            $referentiel->score = 0;
            $referentiel->count  = 0;
            $referentiel->load('audits');
            $referentiel->audits->each(function($audit) use($referentiel){
                $scoreAudit = (new RapportGestionService())->loadScore($audit->id);
                if($scoreAudit){
                    $referentiel->score  += $scoreAudit->score;
                    $referentiel->count  +=1;
                }
            });
            $referentiel->finalScore = $referentiel->count ? ($referentiel->score / $referentiel->count ) : false;
            return $referentiel;
        });
    }

    public function getOneById($id){
        $referentiel =  (new ReferentielUserService)->getOneById($id);
        $referentiel->load('audits');
        $arrayAudit = [];
        $referentiel->audits->each(function($audit) use(&$arrayAudit){
            $test = (new RapportGestionService())->loadScore($audit->id);
            $arrayAudit[] = $test;
        });

        $tableTotal = [];
        $catArray = [];
        foreach($arrayAudit as $audit) {
            $catArray = [];
            $audit->referentiel->category->each(function($cat) use ($catArray)
            if(!isset($catArray[$cat->id])){
                $catArray[$cat->id] = [$audit->dateprevue];
            }
            array_push($catArray[$cat->id],$cat->score);


        }
    }
}
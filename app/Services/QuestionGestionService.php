<?php 

namespace App\Services;

use App\Models\Question;

class QuestionGestionService{

    /**
     * Create and save question
     * @param array $request
     * @return Question
     */
    public function save($request){
        return Question::create([
            'libelle' => $request['libelle'],
            'description' => $request['description'],
            'coef' => $request['coef'],
            'lien' => $request['lien'],
            'categorie_id' => $request['categorie']
        ]);
    }

    /**
     * Update question
     * @param array $request
     * @param int $id
     * @return Question
     */
    public function update($request,$id){
        $question = Question::where('id',$id)->firstOrFail();

        $question->fill([
            'libelle' => $request['libelle'],
            'description' => $request['description'],
            'coef' => $request['coef'],
            'lien' => $request['lien']
        ])->save();
        return $question;
    }


    /**
     * Delete question
     * @param int $id
     * @return bool|object 
     */
    public function delete($id){
        return Question::where('id',$id)->delete();
    }

}
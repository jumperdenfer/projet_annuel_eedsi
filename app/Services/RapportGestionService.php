<?php 

namespace App\Services;

use App\Models\Audit;
use App\Models\Referentiel;
use App\Models\Reponse;

class RapportGestionService{

    /**
     * Calculs des score 
     * @param Audits
     * @return object
     */
    public function loadScore(int $id){
        //Charge complétement un audit
        $audit = Audit::where('id',$id)
        ->with([
            'referentiel.categories' => function($queryQuestion){
                $queryQuestion->with('questions');
            },
            'reponses'
        ])
        ->first();

        $countCategorie = 0;
        $countScore = 0;
        foreach($audit->referentiel->categories as $categorie){
            $scoreCat = $this->calculateScoreByCategorie($categorie,$audit->reponses);
            if($scoreCat !== false){
                $categorie->score = $scoreCat;
                $countCategorie += 1 * $categorie->coef; 
                $countScore += $scoreCat * $categorie->coef;
            }
        }
        $audit->score = $countCategorie !== 0 ? $countScore / $countCategorie : 0;
        return $audit;
    }

    /**
     * Calcul le score de chaque catégorie
     * @param Categorie $categorie
     * @param Reponses $reponses tableau des réponses de l'audit
     * @return object
     */
    private function calculateScoreByCategorie($categorie,$reponses){
        $countQuestion = 0;
        $countScore = 0;
        foreach($categorie->questions as $question){
            $reponse =  $reponses->where('question_id',$question->id)->first();
            if($reponse != null){
                $question->score = $reponse->note * $question->coef;
                $question->reponse = $reponse->note;
                $question->commentaire = $reponse->commentaire;
                $countScore += $question->score;
                $countQuestion += 1 * $question->coef;
            }
        }
        return  $countQuestion != 0 ? (($countScore * 100) / $countQuestion) : false;
    }
}
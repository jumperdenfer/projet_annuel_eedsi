<?php 

namespace App\Services;

use Illuminate\Support\Facades\Auth;

use App\Models\Referentiel;


class ReferentielUserService{

    /**
     * get all referentiel for the Auth User
     * @return array $referentiel
     */
    public function getAllForAuth(){
        return Referentiel::where('user_id',Auth::User()->id)->get();
    }


    /**
     * Get one referentiel by Id
     */
    public function getOneById($id){
        return Referentiel::where('user_id',Auth::User()->id)
        ->where('id',$id)
        ->first();
    }

    /**
     * Create and save a referentiel
     * @param array $request
     * @return Referentiel;
     */
    public function saveReferentiel($request){
        return Referentiel::create([
            'user_id' => Auth::User()->id,
            'libelle' => $request['libelle'],
            'description' => $request['description']
        ]);
    }

    /**
     * Update a existing referentiel 
     * @param array $referentiel
     * @return Referentiel
     */
    public function updateReferentiel($request){
        $referentiel = Referentiel::where('user_id',Auth::User()->id)
        ->where('id',$request['id'])
        ->first();

        $referentiel->fill([
            'libelle' => $request['libelle'],
            'description' => $request['description']
        ])->save();
        return $referentiel;
    }
}
<?php 

namespace App\Services;
use App\Models\Reponse;


class ReponseService{

    public function save($request){
        return Reponse::updateOrCreate(
            [
                'question_id' => $request['question_id'],
                'audit_id' => $request['audit_id']
            ],
            [
                'note' => $request['note'],
                'commentaire' => $request['commentaire']
            ]
        );
    }
}